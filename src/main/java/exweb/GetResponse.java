package exweb;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class GetResponse {

    private Integer draw;
    private Integer recordsFiltered = 50; // hardcoded for testing
    private Integer recordsTotal = 50;    // hardcoded for testing
    private List<List<String>> data = new ArrayList<>();

    public GetResponse(Integer token) {
        this.draw = token;
    }

    public void addRowFrom(Employee employee) {
        data.add(List.of(
                employee.getId().toString(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getCity()));
    }

}
