package exweb;

import com.fasterxml.jackson.databind.ObjectMapper;
import util.FileUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/employee")
public class EmployeeServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException {

        response.setContentType("application/json");

        if (request.getParameter("id") != null) {

            // sample data for on entry
            new ObjectMapper().writeValue(response.getOutputStream(),
                    new Employee(1L, "Alice", "Smith", "London"));

            return;
        }

        DataTableParams tableParams = readParameters(request);

        System.out.println("Parameters from DataTables ajax query: "  + tableParams);
                                        // DataTables expects as to send this token back
        GetResponse r = new GetResponse(tableParams.getToken());

        // sample data for many entries
        r.addRowFrom(new Employee(1L, "Alice", "Smith", "London"));
        r.addRowFrom(new Employee(2L, "Bob", "Smith", "London"));

        new ObjectMapper().writeValue(response.getOutputStream(), r);
    }

    @Override
    public void doPost(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException {

        System.out.println("got POST request with: " +
                FileUtil.readStream(request.getInputStream()));
    }

    @Override
    protected void doPut(HttpServletRequest request,
                         HttpServletResponse response) throws IOException {

        System.out.println("got PUT request with: " +
                FileUtil.readStream(request.getInputStream()));

    }

    @Override
    public void doDelete(HttpServletRequest request,
                            HttpServletResponse response) throws IOException {

        System.out.println("got delete request for id " +
                request.getParameter("id"));
    }

    private DataTableParams readParameters(HttpServletRequest request) {
        DataTableParams params = new DataTableParams();
        params.setOrderColumnNr(parseInt(request.getParameter("order[0][column]")));
        params.setToken(parseInt(request.getParameter("draw")));
        params.setPageStart(parseInt(request.getParameter("start")));
        params.setPageLength(parseInt(request.getParameter("length")));
        params.setSearchString(request.getParameter("search[value]"));
        params.setOrderDirection(request.getParameter("order[0][dir]"));

        return params;
    }

    private Long parseLong(String input) {
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException e) {
            throw new RuntimeException("can't parse long from: " + input, e);
        }
    }

    private Integer parseInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            throw new RuntimeException("can't parse integer from: " + input, e);
        }
    }
}
