package exweb;

import util.DbUtil;
import util.DevDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Tester {

    public static void main(String[] args) {

        DataSource dataSource = new DevDataSource(DbUtil.loadConnectionInfo());

        String sql = "select 42";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();

            rs.next();

            System.out.println(rs.getString(1));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }

}
